package prices

import "github.com/Rhymond/go-money"

// Currency type
type Currency string

const (
	// CZK - Česká koruna
	CZK Currency = "CZK"
	// EUR - Euro
	EUR Currency = "EUR"
)

var (
	//CZKMoney is redeclaration of CZK currency
	CZKMoney = money.AddCurrency("CZK", "K\u010d", "1 $", ",", " ", 2)
	EURMoney = money.AddCurrency("EUR", "\u20ac", "$1", ".", ",", 2)
)

// IsValid returns true for valid currency
func (c Currency) IsValid() bool {
	switch c {
	case CZK, EUR:
		return true
	}

	return false
}
