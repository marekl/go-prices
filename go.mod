module gitlab.com/marekl/go-prices

go 1.15

require (
	github.com/Rhymond/go-money v1.0.2
	github.com/shopspring/decimal v1.2.0
)
