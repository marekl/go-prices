package prices

import (
	"fmt"
	"sort"

	"github.com/Rhymond/go-money"
	"github.com/shopspring/decimal"
)

// Price struct
type Price struct {
	// TODO: Store amount in its fraction
	Value    int64    `gorm:"type:int(64)" json:"value"`
	Currency Currency `gorm:"type:varchar(3);not null" json:"currency"`
	VATRate  VATRate  `gorm:"type:int(2)" json:"vatRate"`
	InclVAT  *bool    `gorm:"type:bool;default:false" json:"inclVAT"`
}

type FinalPrice struct {
	Value    int64    `json:"value"`
	Currency Currency `json:"currency"`
}

// Total holds total for each given VAT rate
type Total struct {
	currency Currency
	values   map[VATRate][]Price
}

// **************************
// * PRICE                  *
// **************************

// Add adds one price to another
func (p Price) Add(ap Price) Price {
	if p.Currency != ap.Currency {
		panic("Cannot mix currencies in Price.Add function")
	}
	if p.VATRate != ap.VATRate {
		panic("Cannot mix VATRates in Price.Add function")
	}
	if *p.InclVAT != *ap.InclVAT {
		panic("Cannot mix price incl. and excl. VAT in Price.Add function")
	}
	return Price{Value: p.Value + ap.Value, Currency: p.Currency, VATRate: p.VATRate, InclVAT: p.InclVAT}
}

// Add adds one price to another
func (p Price) Sub(ap Price) Price {
	if p.Currency != ap.Currency {
		panic("Cannot mix currencies in Price.Sub function")
	}
	if p.VATRate != ap.VATRate {
		panic("Cannot mix VATRates in Price.Sub function")
	}
	if *p.InclVAT != *ap.InclVAT {
		panic("Cannot mix price incl. and excl. VAT in Price.Sub function")
	}
	return Price{Value: p.Value - ap.Value, Currency: p.Currency, VATRate: p.VATRate, InclVAT: p.InclVAT}
}

// Multiply multiplies price by an amount
func (p Price) Multiply(mul int64) Price {
	return Price{Value: p.Value * mul, Currency: p.Currency, VATRate: p.VATRate, InclVAT: p.InclVAT}
}

// Quantity returns price for given amount of products
//
// Quantity is a number with 3 decimal places.
// For example: 1 pc is represented by 1000
func (p Price) Quantity(quantity Quantity) Price {
	quantity000 := decimal.NewFromInt(int64(quantity))
	value000 := decimal.NewFromInt(p.Value).Mul(quantity000)
	value := value000.Shift(-3).IntPart()

	if value000.IntPart()%1000 >= 500 {
		value = value + 1
	}

	return Price{Value: value, Currency: p.Currency, VATRate: p.VATRate, InclVAT: p.InclVAT}
}

// GetVAT returns VAT amount
func (p Price) GetVAT() FinalPrice {
	if *p.InclVAT {
		vat100 := decimal.NewFromInt(100 + int64(p.VATRate))
		base00 := decimal.NewFromInt(p.Value).Div(vat100).Shift(4)
		base := base00.Shift(-2).IntPart()

		if base00.IntPart()%100 >= 50 {
			base = base + 1
		}

		return FinalPrice{Value: p.Value - base, Currency: p.Currency}
	}

	VAT00 := p.Value * int64(p.VATRate)

	VAT := decimal.NewFromInt(VAT00).Shift(-2).IntPart()
	if VAT00%100 >= 50 {
		VAT = VAT + 1
	}

	return FinalPrice{Value: VAT, Currency: p.Currency}
}

// GetBase returns base price excluding VAT
func (p Price) GetBase() FinalPrice {
	if *p.InclVAT {
		inclVAT := FinalPrice{Value: p.Value, Currency: p.Currency}
		return inclVAT.Sub(p.GetVAT())
	}

	return FinalPrice{Value: p.Value, Currency: p.Currency}
}

// GetInclVAT returns price including VAT
func (p Price) GetInclVAT() FinalPrice {
	if *p.InclVAT {
		return FinalPrice{Value: p.Value, Currency: p.Currency}
	}
	return p.GetBase().Add(p.GetVAT())
}

// **************************
// * FINAL PRICE            *
// **************************

// GetMoney returns Money struct from Price
func (f *FinalPrice) GetMoney() *money.Money {
	return money.New(f.Value, string(f.Currency))
}

// Add adds one price to another
func (f FinalPrice) Add(af FinalPrice) FinalPrice {
	if f.Currency != af.Currency {
		panic("Cannot mix currencies in Price.Add function")
	}
	return FinalPrice{Value: f.Value + af.Value, Currency: f.Currency}
}

// Sub subtracts one price from another
func (f FinalPrice) Sub(af FinalPrice) FinalPrice {
	if f.Currency != af.Currency {
		panic("Cannot mix currencies in Price.Add function")
	}
	return FinalPrice{Value: f.Value - af.Value, Currency: f.Currency}
}

// Format Price object
func (f FinalPrice) Format() string {
	return f.GetMoney().Display()
}

// Format Price object
func (f FinalPrice) AsMajorUnits() string {
	return fmt.Sprintf("%.*f", f.GetMoney().Currency().Fraction, f.GetMoney().AsMajorUnits())
}

// Multiply multiplies price by an amount
func (f FinalPrice) Multiply(mul int64) FinalPrice {
	return FinalPrice{Value: f.Value * mul, Currency: f.Currency}
}

// Quantity returns price for given amount of products
//
// Quantity is a number with 3 decimal places.
// For example: 1 pc is represented by 1000
func (f FinalPrice) Quantity(quantity Quantity) FinalPrice {
	quantity000 := decimal.NewFromInt(int64(quantity))
	value000 := decimal.NewFromInt(f.Value).Mul(quantity000)
	value := value000.Shift(-3).IntPart()

	if value000.IntPart()%1000 >= 500 {
		value = value + 1
	}

	return FinalPrice{Value: value, Currency: f.Currency}
}

// **************************
// * TOTAL                  *
// **************************

// GetVATRate returns subtotal for given VATRate
func (t Total) GetItems(rate VATRate) (items []Price) {
	var ok bool
	if items, ok = t.values[rate]; !ok {
		return make([]Price, 0)
	}
	return
}

// Add adds price to the total
func (t *Total) Add(price Price) {
	if t.values == nil {
		t.values = make(map[VATRate][]Price)
	}
	if t.currency == "" {
		t.currency = price.Currency
	}

	currentItems := t.GetItems(price.VATRate)
	t.values[price.VATRate] = append(currentItems, price)
}

func (t Total) GetBase(rate VATRate) FinalPrice {
	base := FinalPrice{Value: 0, Currency: t.currency}

	for _, item := range t.GetItems(rate) {
		base = base.Add(item.GetBase())
	}

	return base
}

func (t Total) GetVAT(rate VATRate) FinalPrice {
	vat := FinalPrice{Value: 0, Currency: t.currency}

	for _, item := range t.GetItems(rate) {
		vat = vat.Add(item.GetVAT())
	}

	return vat
}

func (t Total) GetInclVAT(rate VATRate) FinalPrice {
	total := FinalPrice{Value: 0, Currency: t.currency}

	for _, item := range t.GetItems(rate) {
		total = total.Add(item.GetBase()).Add(item.GetVAT())
	}

	return total
}

func (t Total) GetTotalBase() FinalPrice {
	base := FinalPrice{Value: 0, Currency: t.currency}

	for rate := range t.values {
		base = base.Add(t.GetBase(rate))
	}

	return base
}

func (t Total) GetTotalVAT() FinalPrice {
	total := FinalPrice{Value: 0, Currency: t.currency}

	for rate := range t.values {
		total = total.Add(t.GetVAT(rate))
	}

	return total
}

func (t Total) GetTotalInclVAT() FinalPrice {
	total := FinalPrice{Value: 0, Currency: t.currency}

	for rate := range t.values {
		total = total.Add(t.GetInclVAT(rate))
	}

	return total
}

// GetVATRates returns sorted list of VATRates present in Total values
func (t Total) GetVATRates() []VATRate {
	numRates := make([]float64, 0, len(t.values))
	for rate := range t.values {
		numRates = append(numRates, float64(rate))
	}
	sort.Float64s(numRates)
	rates := make([]VATRate, len(numRates))
	for i, rate := range numRates {
		rates[i] = VATRate(rate)
	}
	return rates
}
