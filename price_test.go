package prices

import "testing"

func TestQuantity(t *testing.T) {
	f := false

	originalPrice := Price{Value: 10, Currency: CZK, VATRate: BasicVATRate, InclVAT: &f}

	roundDown := Quantity(40)
	roundUp := Quantity(50)

	if price := originalPrice.Quantity(roundDown); price.Value != 0 {
		t.Errorf(`Price %d should get rounded down to zero with quantity %d. Recieved %d,`, originalPrice.Value, roundDown, price.Value)
	}

	if price := originalPrice.Quantity(roundUp); price.Value == 0 {
		t.Errorf(`Price %d should get rounded up to 1 with quantity %d. Recieved %d,`, originalPrice.Value, roundUp, price.Value)
	}

	finalPrice := FinalPrice{Value: 10, Currency: CZK}

	if price := finalPrice.Quantity(roundDown); price.Value != 0 {
		t.Errorf(`FinalPrice %d should get rounded down to zero with quantity %d. Recieved %d,`, finalPrice.Value, roundDown, price.Value)
	}

	if price := finalPrice.Quantity(roundUp); price.Value == 0 {
		t.Errorf(`FinalPrice %d should get rounded up to 1 with quantity %d. Recieved %d,`, finalPrice.Value, roundUp, price.Value)
	}

}

func TestAsMajorUnit(t *testing.T) {
	testCases := map[int64]string{
		0:     "0.00",
		1:     "0.01",
		10:    "0.10",
		100:   "1.00",
		12345: "123.45",
	}

	for input, expectedOutput := range testCases {
		price := FinalPrice{Value: input, Currency: CZK}

		if output := price.AsMajorUnits(); output != expectedOutput {
			t.Errorf(`FinalPrice with value %d should output %s for AsMajorUnit function. Recieved %s,`, price.Value, expectedOutput, output)
		}
	}
}
