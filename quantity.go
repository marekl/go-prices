package prices

import (
	"strconv"
	"strings"
)

const QUAN_NUM = 3
const DECIMAL_SEP = ","

type Quantity int64

// abs return absolute value of quantity
func (q Quantity) abs() Quantity {
	if q < 0 {
		return -q
	}
	return q
}

// Format returns formatted quantity string
func (q Quantity) Format() string {
	formatted := strconv.FormatInt(int64(q.abs()), 10)

	// Pad left with zeros + 1 leading zero
	if len(formatted) <= QUAN_NUM {
		formatted = strings.Repeat("0", QUAN_NUM-len(formatted)+1) + formatted
	}

	// Add decimal separator
	formatted = formatted[:len(formatted)-QUAN_NUM] + DECIMAL_SEP + formatted[len(formatted)-QUAN_NUM:]

	// Add minus sign for negative amount.
	if q < 0 {
		formatted = "-" + formatted
	}

	// Remove trailing zeros
	formatted = strings.TrimRight(formatted, "0")
	// Trim trailing separator
	formatted = strings.TrimRight(formatted, DECIMAL_SEP)

	return formatted
}
