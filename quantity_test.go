package prices

import "testing"

func TestQuantityFormat(t *testing.T) {

	testSubjects := map[Quantity]string{
		0:    "0",
		1000: "1",
		100:  "0,1",
		10:   "0,01",
		1:    "0,001",
		1111: "1,111",
	}

	for quantity, quantityString := range testSubjects {
		if quantity.Format() != quantityString {
			t.Errorf(`Quantity %d should be formatted as %s but recieved %s instead`, quantity, quantityString, quantity.Format())
		}
	}

}
