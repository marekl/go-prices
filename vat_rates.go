package prices

// VATRate type
type VATRate int32

const (
	ZeroVATRate          VATRate = 0
	SecondReducedVATRate VATRate = 10
	FirstReducedVATRate  VATRate = 15
	BasicVATRate         VATRate = 21
)
